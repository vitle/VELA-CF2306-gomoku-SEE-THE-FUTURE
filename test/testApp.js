const assert= require('chai').assert;

const app =  require('../app');
let user1 = {
    turn: 1,
    steps: []
}
let user2 = {
    turn: 2,
    steps: []
}
let countTurn = 1
describe('Gomoku', function(){
    // it('Hai nguoi khong duoc di lien tiep', function(){
    //     countTurn = 5
    //     let result = app.checkUserHaveTwoConsecutive(countTurn,user2)
    //     assert.equal(result,false);
    // })

    // it('Khong duoc di vao o da di roi', function() {
    //     user1.steps = [[1,1],[1,2],[1,3]]
    //     user2.step = [[2,1],[2,2]]
    //     let result = app.checkNotSamePosition(user1,user2,[1,2])
    //     assert.equal(result,false);
    // })

    // it('Truong hop win ngang', function() {
    //     user1.steps = [[1,3],[3,3],[4,3],[5,3]]
    //     let result = app.checkWinHorizontalRow(user1, [2,3])
    //     assert.equal(result,true);
    // })

    // it('Truong hop win doc', function() {
    //     user1.steps = [[1,3],[1,2],[1,4],[1,5]]
    //     let result = app.checkWinVerticalRow(user1, [1,6])
    //     assert.equal(result,true);
    // })
    // it('Truong hop win cheo phai', function() {
        
    //     user1.steps = [[1,1],[2,2],[3,3],[4,4]]
    //     let result = app.checkWinRightDiagonalRow(user1, [5,5])
    //     assert.equal(result,true);
    // })

    // it('Truong hop win cheo trai', function() {
    //         user1.steps = [[1,5],[2,4],[3,3],[4,2]]
    //     let result = app.checkWinLeftDiagonalRow(user1, [5,1])
    //     assert.equal(result,true);
    // })

    it('Truong hop 4 trang ngang', function(){
        user1.steps = [[3,3],[4,3],[5,3]]
        user2.steps = []
        let result = app.check4WinHorizontalRow(user1,user2,[2,3])
        assert.equal(result,true);
    })

    it('Truong hop 4 trang ngang bi chan', function(){
        user1.steps = [[3,3],[4,3],[5,3]]
        user2.steps = [[6,3]]
        let result = app.check4WinHorizontalRow(user1,user2,[2,3])
        assert.equal(result,false);
    })

    it('Truong hop 4 trang doc', function(){
        user1.steps = [[1,3],[1,4],[1,5]]
        user2.steps = []
        let result = app.check4WinVerticalRow(user1,user2,[1,6])
        assert.equal(result,true);
    })

    it('Truong hop 4 trang doc bi chan', function(){
        user1.steps = [[1,3],[1,4],[1,5]]
        user2.steps = [[1,2]]
        let result = app.check4WinVerticalRow(user1,user2,[1,6])
        console.log(result)
        assert.equal(result,false);
    })

})