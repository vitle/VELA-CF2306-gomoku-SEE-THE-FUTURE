module.exports = {
    horizontalRow: function(user1, nextPosition) {
        let arrPosition = []
        arrPosition.push(nextPosition)
        let positionY = nextPosition[1]
            let positionX = nextPosition[0]
            let count = 1
            // check tien
            let flag = true
            while(flag){
                let exist = false
                let nextX = positionX + count
                let positionNew = [nextX, positionY]
                for(let i in user1.steps){
                    let item = user1.steps[i]
                    if(JSON.stringify(item) == JSON.stringify(positionNew)){
                        arrPosition.push(positionNew)
                        count++
                        exist = true
                    }
                }
                if(!exist)
                    flag  = false
            }
            // lui
            count = 1
            flag = true
            while(flag){
                let exist = false
                let preX = positionX - count
                let positionNew = [preX, positionY]
                for(let i in user1.steps){
                    let item = user1.steps[i]
                    if(JSON.stringify(item) == JSON.stringify(positionNew)){
                        arrPosition.push(positionNew)
                        count++
                        exist = true
                    }
                }
                if(!exist)
                    flag  = false
            }
            return arrPosition
    },

    verticalRow : function (user1, nextPosition) {
        let positionY = nextPosition[1]
        let positionX = nextPosition[0]
        let count = 1
        let arrPosition = []
        arrPosition.push(nextPosition)
        // check tien
        let flag = true
        while(flag){
            let exist = false
            let nextY = positionY + count
            let positionNew = [positionX, nextY]
            for(let i in user1.steps){
                let item = user1.steps[i]
                if(JSON.stringify(item) == JSON.stringify(positionNew)){
                    arrPosition.push(positionNew)
                    count++
                    exist = true
                }
            }
            if(!exist)
                flag  = false
        }
        // lui
        count = 1
        flag = true
        while(flag){
            let exist = false
            let preY = positionY - count
            let positionNew = [positionX, preY]
            for(let i in user1.steps){
                let item = user1.steps[i]
                if(JSON.stringify(item) == JSON.stringify(positionNew)){
                    arrPosition.push(positionNew)
                    count++
                    exist = true
                }
            }
            if(!exist)
                flag  = false
        }

        return arrPosition
    },

    rightDiagonalRow : function (user1, nextPosition) {
        let positionY = nextPosition[1]
        let positionX = nextPosition[0]
        let count = 1
        let arrPosition = []
        arrPosition.push(nextPosition)
        // check tien
        let flag = true
        while(flag){
            let exist = false
            let nextY = positionY + count
            let nextX = positionX + count
            let positionNew = [nextX, nextY]
            for(let i in user1.steps){
                let item = user1.steps[i]
                if(JSON.stringify(item) == JSON.stringify(positionNew)){
                    arrPosition.push(positionNew)
                    count++
                    exist = true
                }
            }
            if(!exist)
                flag  = false
        }
        // lui
        count = 1
        flag = true
        while(flag){
            let exist = false
            let preX = positionX - count
            let preY = positionY - count
            let positionNew = [preX, preY]
            for(let i in user1.steps){
                let item = user1.steps[i]
                if(JSON.stringify(item) == JSON.stringify(positionNew)){
                    arrPosition.push(positionNew)
                    count++
                    exist = true
                }
            }
            if(!exist)
                flag  = false
        }
        
        return arrPosition
    },

    leftDiagonalRow : function (user1, nextPosition) {

        let positionY = nextPosition[1]
        let positionX = nextPosition[0]
        let count = 1
        let arrPosition = []
        arrPosition.push(nextPosition)
        // check tien
        let flag = true
        while(flag){
            let exist = false
            let nextY = positionY - count
            let nextX = positionX + count
            let positionNew = [nextX, nextY]
            for(let i in user1.steps){
                let item = user1.steps[i]
                if(JSON.stringify(item) == JSON.stringify(positionNew)){
                    arrPosition.push(positionNew)
                    count++
                    exist = true
                }
            }
            if(!exist)
                flag  = false
        }
        // lui
        count = 1
        flag = true
        while(flag){
            let exist = false
            let preX = positionX - count
            let preY = positionY + count
            let positionNew = [preX, preY]
            for(let i in user1.steps){
                let item = user1.steps[i]
                if(JSON.stringify(item) == JSON.stringify(positionNew)){
                    arrPosition.push(positionNew)
                    count++
                    exist = true
                }
            }
            if(!exist)
                flag  = false
        }
    

        return arrPosition
    },

    checkHeadAndRetailHorizontal: function(arrCanWin, user2) {
        let maxX = 0
            let minX = 0
            for(let i in arrCanWin){
                let item = arrCanWin[i]
                if(maxX < item[0])
                    maxX = item[0]
                if(minX > item[0])
                    minX = item[0]
            }
            let itemNextMaxX = [maxX+1, arrCanWin[0][1]]
            let itemNextMinX = [minX -1 , arrCanWin[0][1]]

            for(let i in user2.steps){
                let item = user2.steps[i]
                if(JSON.stringify(item) == JSON.stringify(itemNextMaxX) || JSON.stringify(item) == JSON.stringify(itemNextMinX))
                    return false
            }
            return true
    },

    checkHeadAndRetailVertical: function(arrCanWin, user2) {
        let maxY = 0
            let minY = 0
            for(let i in arrCanWin){
                let item = arrCanWin[i]
                if(maxY < item[1])
                    maxY = item[1]
                if(minY > item[1])
                    minY = item[1]
            }
            let itemNextMaxY = [arrCanWin[0][0], maxY +1 ]
            let itemNextMinY = [arrCanWin[0][0], minY -1 ]

            for(let i in user2.steps){
                let item = user2.steps[i]
                if(JSON.stringify(item) == JSON.stringify(itemNextMaxY) || JSON.stringify(item) == JSON.stringify(itemNextMinY))
                    return true
            }
            return false
    },

    
}
