var util = require('./util');
module.exports = {
    delta: function(a, b) {
        return a+b
    },
    checkUserHaveTwoConsecutive: function(user, countTurn){
        if(countTurn%2 != user.turn)
            return false
        return true
    },

    checkNotSamePosition: function(user1,user2, nextPosition){
        let listPosition = user1.steps.concat(user2.steps)
        for(let i in listPosition){
            let item = listPosition[i]
            if(JSON.stringify(item) == JSON.stringify(nextPosition))
                return false
        }
        return true
    },

    checkWinHorizontalRow : function (user1, nextPosition) {
        let arrCanWin = util.horizontalRow(user1, nextPosition)
        if (arrCanWin.length == 5){
            return true;
        }
        return false
    },

    checkWinVerticalRow : function (user1, nextPosition) {
        let arrCanWin = util.verticalRow(user1, nextPosition)
        if (arrCanWin.length == 5){
            return true;
        }
        return false
    },

    checkWinRightDiagonalRow : function (user1, nextPosition) {
        let arrCanWin = util.rightDiagonalRow(user1, nextPosition)
        if (arrCanWin.length == 5){
            return true;
        }

        return false
    },

    checkWinLeftDiagonalRow : function (user1, nextPosition) {
        let arrCanWin  = util.rightDiagonalRow
        
        if (arrCanWin.length == 5){
            return true;
        }

        return false
    },

    checkWinLeftDiagonalRow : function (user1, nextPosition) {
        let arrCanWin = util.leftDiagonalRow(user1, nextPosition)
        if (arrCanWin.length == 5){
            return true;
        }

        return false
    },

    check4WinHorizontalRow : function (user1,user2, nextPosition) {
        let arrCanWin = util.horizontalRow(user1, nextPosition)
        if (arrCanWin.length == 4){
            return util.checkHeadAndRetailHorizontal(arrCanWin, user2)
        }
        return true
    },

    check4WinVerticalRow : function (user1, user2, nextPosition) {
        let arrCanWin = util.verticalRow(user1, nextPosition)
        if (arrCanWin.length == 4){
            console.log("check" +util.checkHeadAndRetailVertical(arrCanWin, user2))
            return util.checkHeadAndRetailVertical(arrCanWin, user2);
        }
        return false
    },

    // checkWinRightDiagonalRow : function (user1, nextPosition) {
    //     let arrCanWin = util.rightDiagonalRow(user1, nextPosition)
    //     if (arrCanWin.length == 5){
    //         return true;
    //     }

    //     return false
    // },

    // checkWinLeftDiagonalRow : function (user1, nextPosition) {
    //     let arrCanWin  = util.rightDiagonalRow
        
    //     if (arrCanWin.length == 5){
    //         return true;
    //     }

    //     return false
    // },

    // checkWinLeftDiagonalRow : function (user1, nextPosition) {
    //     let arrCanWin = util.leftDiagonalRow(user1, nextPosition)
    //     if (arrCanWin.length == 5){
    //         return true;
    //     }

    //     return false
    // }




}